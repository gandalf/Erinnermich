# Erinnermich

Teil 3 meines eigenen Aktenverwaltungssystems.
Überblick über Wiedervorlagen, Fristen und Termine halten.

## Daten

```
Wiedervorlagen.csv
==================
Datum,Typ,rec,erledigt
```
Diese enthält die Daten, die von `erinnermich-collect.sh` in die zentrale `Wiedervorlagen.md` kompiliert werden. 
Im Internformat möglicherweise unsortiert und möglicherweise alte Termine. Evtl. werden auch Wiedervorlagen, die keine Fristsachen sind,
nicht im Vorraus in die Übersicht einsortiert, mal schauen.

## Skripte

#### `erinnermich.sh`
Füllt die Datei `Wiedervorlagen.csv` mit Nutzieingaben.

#### `erinnermich-collect.sh`
Sammelt bei jedem Rechnerneustart (`cron@boot`) aus der gesamten Dateistruktur alle Wiedervorlagen zusammen und baut eine
Markdown-formatierte Übersicht. Oder ein anderes Format? An sich könnte er das auch per `LaTeX` oder als `svg` in das Hintergrundbild schummeln…
Holt sich die Aktenzeichen aus `DECKBLATT.csv`

Durchsucht standardmäßig `/home/user/Dokumente/`, Änderungen daran können in `.env` vorgenommen werden.

Optionen:
* `-o md|pdf|csv|?` Ausgabeformat. Default ist `md`
* `-f future|past|all` Ausgabe von Fristen, default ist `future`
* `-t future|past|all` Ausgabe von Terminen, default ist `future`
* `-w future|done|open|all` Ausgabe von Wiedervorlagen, default ist `open`: Stichtag ist vorbei, aber noch nicht als bearbeitet vermerkt.
* `-m, --merge` Fristen, Termine und Wiedervorlagen in eine Tabelle sammeln. Default sind drei eigene Tabellen in einer Datei.
* `-a` äquivalent zu `-t all -f all -w all`. Überschreibt vorherige `-t`, `-w`, `-f` (und wird von späteren überschrieben)

## Ausgabe

Eine durch `erinnermich-collect.sh` per cron@boot erstellte Übersicht der nächsten Termine:
```
Wiedervorlagen.md
=================
Datum | Typ | Gegenseite/Gericht | deren Az | mein Az | PATH
------------------------------------------------------------
13.12.21 | harte Frist | AG Düren | 13 Js 43/21 | Bullshit/21 | ~/Dokumente/Strafrecht/Bullshit
15.12.21 | weiche Frist | VG Köln | 5 K 161/20 | Blubb/20 | ~/Dokumente/Verwaltungsklagen/Blubb
24.12.21 | Wiedervorlage | IM NRW | 352351354-18 | Schleierfahndung | ~/Dokumente/Recherche/Hambi
6.1.22   | Hauptverhandlung| AG Düren | 13 Js 43/21 | Bullshit/21 | ~/Dokumente/Strafrecht/Bullshit
```
