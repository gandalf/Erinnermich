#! /usr/bin/env bash

#Prepare Flags and Variables
OF="md"
FF="future"
WF="open"
MF=false
TF="future"
ARGS=($@)
FILES=()
BASEPATH=$(dirname $(realpath $0))
. .env # FINDPATH

#Read command line input
for (( ARG=0; ARG<$#; ARG++ ))
do
	VAL=${ARGS[$ARG]}
	case $VAL in
		"-m") MF=true;;
		"-f") FF=${ARGS[$ARG+1]};;
		"-w") WF=${ARGS[$ARG+1]};;
		"-t") TF=${ARGS[$ARG+1]};;
		"-a") TF="all"; WF="all"; FF="all";;
		"-am") TF="all"; WF="all"; FF="all"; MF=true;;
	esac
done

#find data
FILES=$(find $FINDPATH -iname ".erinnermich.csv")

#collect data into one file
echo "date,type,handle,done,comment,myaz,az,dir" > .erinneralle.csv # Lösche Sammeldatei und lege mit Kopfzeile neu an
for FILE in $FILES
do
	if [[ $(head -n 1 $FILE) =~ "date,type,handle,done,comment" ]]
	then
		DIR=$(dirname $FILE)
		AZ=$(grep "+$" $DIR/DECKBLATT.csv | cut -d, -f3)
		MYAZ=$(grep "^ME" $DIR/DECKBLATT.csv | cut -d, -f3)
		tail -n +2 $FILE | sed "s:$:,$MYAZ,$AZ,$DIR:" >> $BASEPATH/.erinneralle.csv
	fi
done

set -f
IFS=$'\n' TS="$(egrep "^[+:T0-9-]+,T," $BASEPATH/.erinneralle.csv | sort)"
IFS=$'\n' FS="$(egrep "^[+:T0-9-]+,F," $BASEPATH/.erinneralle.csv | sort)"
IFS=$'\n' fS="$(egrep "^[+:T0-9-]+,f," $BASEPATH/.erinneralle.csv | sort)"
IFS=$'\n' WS="$(egrep "^[+:T0-9-]+,W," $BASEPATH/.erinneralle.csv | sort)"


