#!/bin/bash

### This shell script is used to create new events in caldav on the owncloud server
### Copied from https://gist.github.com/remyd1/a353b07219884c5878cc27d04aab9d96 and modified to fit my needs.


# Usage output string. $0 is filename.
usage="$0 categories date description [dtstart] [dtend]
where categories is a comma separated list that MUST start with FRIST, WV or TERMIN
and the second item in this list denotes the required alarm set."

# print usage string if improper options, or if requested.
if [ "$#" -lt 3 ] || [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
    echo "$usage"
    exit 1
else

# read login credentials and config
BASEPATH=$(dirname $(realpath $0))
. $BASEPATH/.env

# generate random UID starting here
    random1=`</dev/urandom tr -dc '1-9a-z' | head -c8; echo ""`
    random2=`</dev/urandom tr -dc '1-9a-z' | head -c4; echo ""`
    random3=`</dev/urandom tr -dc '1-9a-z' | head -c4; echo ""`
    random4=`</dev/urandom tr -dc '1-9a-z' | head -c4; echo ""`
    random5=`</dev/urandom tr -dc '1-9a-z' | head -c12; echo ""`

    # UID=`</dev/urandom tr -dc '1-9a-z' | head -c10; echo ""`

    # ics example 8cbf7d9e-6g68-43b9-zb3c-073a8e6b8f46.ics
    UUID=$random1-$random2-$random3-$random4-$random5
# done here generating an UID
    NOW=`date +%Y%m%dT%H%M%S`

    
# find main category and decide for a template
    main_cat=$(echo $1 | cut -d, -f1 | tr 'A-Z' 'a-z')

# last preparations of arguments to pass to the function:
inputcat=$1
inputdate=$2
inputdesc=$3
inputstart=$4
inputend=$5

prepare_file() {
    local mode=${1,?Mode is required}
    template=$BASEPATH/templates/template_${main_cat}_${mode}.ics
    ics=$UUID-$mode.ics

if [ -e $template ]; then
# initiate iCal file with template
    cp $template $ics

# and change some stuff with cli options.
# what we need to change:
# dtstamp, lastmodified, created:
    sed -i "s|20160224T172807|$NOW|g" $ics
# date (this script will only add single day events. Use other calDAV front-ends for more sophisticated usage)
    sed -i "s|20160225|$inputdate|g" $ics
# categories
    sed -i "s|replacemecategory|$inputcat|g" $ics
# replacemealarm
# We have to do quite a lot of shitty magic here to get the right representations for the different kinds of newlines.
    alarm=$($BASEPATH/add_alarm.sh $mode $inputcat $inputdate) 
    alarm=${alarm//$'\n'/\\$'\n'}
    sed -i "s|replacemealarm|${alarm//\\n/\\\\n}|g" $ics
# AktenzeichenXYZ
# take from deckblatt 
    aktenzeichen=""
    if [ -e DECKBLATT.csv ]; then
	    azme=$(grep "0$" DECKBLATT.csv | cut -d, -f3)
	    azthem=$(grep "+$" DECKBLATT.csv | cut -d, -f3)
	    aktenzeichen="$azme -- $azthem"
    fi
    sed -i "s|AktenzeichenXYZ|$aktenzeichen|g" $ics
# human-readable date
    sed -i "s|replacemedate|$(date -d $inputdate +%d.%m.%Y), 2300|g" $ics
# description:
    sed -i "s|replacemedescription|${inputdesc//$'\n'/\\\\n}|g" $ics
# what we might need to replace, but not even that often:
# dtstart, time component optional. We only work with minutes here:
    [ -n "$inputstart" ] && echo ${#inputstart}
    [ -n "$inputstart" ] && sed -i "s|2300|$inputstart|g" $ics
# dtend, same as dtstart:
    [ -n "$inputend" ] && sed -i "s|2359|$inputend|g" $ics
# UUID:
    sed -i "s|99g999gggg|$UUID|g" $ics

    domain=${domain/%\//}
    user=$username:$secret
    url=$domain/$username/$calendar

    echo curl -k --user $user -X PUT -H "Content-Type: text/calendar; charset=utf-8" --data-binary @./$ics --url $url/$ics
    curl -k --user $user -X PUT -H "Content-Type: text/calendar; charset=utf-8" --data-binary @./$ics --url $url/$ics
fi
}

prepare_file t
prepare_file e

fi
