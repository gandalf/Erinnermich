#! /bin/bash

# This is a front-end to make the use of add_date.sh more convenient

function usage {
	echo "Usage: 
        $ $0 <category_options> [-d <date>|-P <relativ date>] <description> [-tstart <value>] [-tend <value>]
	* <category_options> MUST contain EXACTLY ONE of -frist, -wv or -termin
	and can be expanded by e.g. -jura, -personal, ...
	Expansion options need to be specified in in your .env first.
	* values for -tstart and -tend are military time (HHMM)
	* date can be any format that is recognised by the standard tool of the same name.
	* EXACTLY ONE of -d and -P is REQUIRED. If both are given, it is not defined which will be passed on.

	$ $0 -help
	Print this Message and exit"
	exit 1 
}

#Parsing Parameters:
BASEPATH=$(dirname $(realpath $0))
. $BASEPATH/.env
unset $GETOPT_COMPATIBLE
ARGV=$(getopt -n "$0" -a -o "d:P:" -l "frist,wv,termin,$catopts,help,tstart:,tend:" -- "$@")
if [ $? -ne 0 ]
then
	usage
fi
eval set -- "$ARGV"
CATEGORIES=
#Now we have $@ clean and tidy and begin parsing
while :
do
	case "$1" in
		"--help") usage ;;
		"--frist") 
			CATEGORIES=FRIST,$CATEGORIES
			shift ;;
		"--termin") 
			CATEGORIES=TERMIN,$CATEGORIES
			shift ;;
		"--wv") 
			CATEGORIES=WV,$CATEGORIES
			shift ;;
		"--tstart")
			tstart=$2
			shift ;;
		"--tend")
			tend=$2
			shift ;;
		"--"?*)
			CATEGORIES=$CATEGORIES$(tr 'a-z' 'A-Z' <<< ${1/--/}),
			shift ;;
		"-d")   
			date=$(date -d "$2" +%Y%m%d)
			shift
			shift ;;
		"-P")
			date=$(date -d "${${${${2/Y/ years}/m/ months}/W/ weeks}/D/ days}" +%Y%m%d)
			shift
			shift ;;
		"--")
			shift
			break ;;
	esac
done
#letztes Komma aus CATEGORIES streichen
CATEGORIES=${CATEGORIES/%,/}
echo $BASEPATH/add_date.sh $CATEGORIES $date "$1" $tstart $tend
$BASEPATH/add_date.sh $CATEGORIES $date "$1" $tstart $tend
